let React = require("react");
let ReactDOM = require("react-dom");
let styled = require("styled-components").default;
let media = require("styled-media-query").default;


let Database = require("database-api").default;
let config = require("visual-config-exposer").default.postGameScreen;

let database = new Database();

function getCardHeight() {
    let height = 75;
    if (!config.showLeadeboardButtonPostGameScreen) {
        return height;
    }
    height += 80;
    if (config.submitName) {
        height += 60;
    }
    if (config.submitEmail) {
        height += 60;
    }
    return height;
}

const cardWidth = window.mobile() ? "280px" : "350px";

const Card = styled.div`
    background-color: ${(props) => props.bgColor};
    width: ${cardWidth};
    text-align: center;
    position: fixed;
    left: 50%;
    transform: translate(-50%, 8%);
    -webkit-transform: translate(-50%, 8%);
    -moz-transform: translate(-50%, 8%);
    -o-transform: translate(-50%, 8%);
    -ms-transform: translate(-50%, 8%);
    box-sizing: border-box;
    margin: auto;
    padding: 10px;
    height: ${(props) => props.height};
    border-radius: 30px;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
    @media (max-width: 480px) {
        position: static;
        transform: translate(0, 0);
        -webkit-transform: translate(0, 0);
        -moz-transform: translate(0, 0);
        -o-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
        width: 95%;
        height: auto;
    }

    ${(props) => props.extra}
`;

const Text = styled.div`
    font-size: ${(props) => props.fontSize};
    color: ${(props) => props.fontColor};
    ${(props) => props.extra}
    @media (max-width: 370px) {
        box-sizing: border-box;
        font-size: 6vw;
    }
`;

const SubmitBox = styled.input`
    font-size: ${(props) => props.fontSize};
    color: ${(props) => props.fontColor};
    padding: 3px;
    ${(props) => props.extra}
    @media (max-width: 370px) {
        box-sizing: border-box;
        font-size: 6vw;
        padding: 16px;
        height: auto;
    }
`;

const Button = styled.button`
    width: ${(props) => (props.width ? props.width : "60%")};
    height: ${(props) => (props.height ? props.height : "15%")};
    border-radius: 20px;
    font-size: ${(props) => props.fontSize};
    background-color: ${(props) => (props.bgColor ? props.bgColor : config.buttonColor)};
    color: ${(props) => (props.fontColor ? props.fontColor : config.buttonTextColor)};
    border: none;
    outline: none;
    ${(props) => props.extra}
    @media (max-width: 370px) {
        box-sizing: border-box;
        font-size: 5vw;
        padding: 16px;
        height: auto;
    }
`;

const Table = styled.table`
width: 100%;
background-color: #f7f5f5;
overflow: scroll;
margin-top: 10px;
border-radius: 10px;
`;

const Tbody = styled.tbody`
display: block;
height: ${ props => props.height };
overflow-y: scroll;
table-layout: auto;
width: 100%;
@media (max-width: 370px) {
    height: 180px;
}
`;

const Tr = styled.tr``;

const Td = styled.td`
    ${(props) => props.extra}
`;

class Leaderboard extends React.Component {
    render() {
        return (
            <div>
                <Text fontSize="1.4rem" fontColor={config.textColor}>
                    Leaderboard
                </Text>

                <Table>
                    <Tbody height={this.props.height + "px; font-size: 1rem"}>
                        {this.props.data.map((entry, index) => {
                            return (
                                <Tr key={index}>
                                    <Td extra={`width: ${cardWidth}`}>{index + 1 + ". " + entry.display_name}</Td>
                                    <Td extra="text-align: right;">{entry.score}</Td>
                                </Tr>
                            );
                        })}
                    </Tbody>
                </Table>
            </div>
        );
    }
}

class PostGameScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLeaderboard: false,
            leaderboardData: null,
            score: window.score,
            name: "",
            email: "",
        };
    }

    render() {
        const card1Height = getCardHeight();
        const card2Height = 65;
        let card3Height = 0;
        const leaderboardHeight = 320;

        if (config.showText) {
            card3Height += 78;
        }
        if (config.showCTA) {
            card3Height += 100;
        }

        let card1Top = config.showText || config.showCTA ? -(card1Height + card2Height + card3Height) * 0.6 : 0;
        if (!config.showLeadeboardButtonPostGameScreen) {
            card1Top -= card1Height * 2;
        }
        const card2Top = card1Top + (this.state.showLeaderboard ? leaderboardHeight : card1Height) + 330;
        const card3Top = card2Top + card2Height + card3Height + 90;

        return (
            <div>
                <div>&nbsp;</div>
                {this.state.showLeaderboard == true && (
                    <Card bgColor={config.cardColor} height={leaderboardHeight} extra={`top: 0;padding: 20px !important;`}>
                        {this.state.leaderboardData != null && (
                            <Leaderboard
                                height={leaderboardHeight - 70}
                                data={this.state.leaderboardData}
                            ></Leaderboard>
                        )}
                    </Card>
                )}
                {this.state.showLeaderboard == false && (
                    <Card bgColor={config.cardColor} height={card1Height + "px"} extra={`top: 10%`}>
                        <Text fontSize="1.6rem" fontColor={config.textColor}>
                            {config.scoreText + window.score}
                        </Text>
                        {config.showLeadeboardButtonPostGameScreen && (
                            <div>
                                {config.submitName && (
                                    <div>
                                        <Text
                                            fontSize="1.2rem"
                                            fontColor={config.textColor}
                                            extra=" margin-top: 20px; text-align: left; margin-left: 30px;"
                                        >
                                            Twitter Username:
                                        </Text>
                                        <SubmitBox
                                            fontSize="0.9rem"
                                            fontColor={config.textColor}
                                            extra="width: 80%;"
                                            onChange={(ev) => {
                                                this.setState({ name: ev.target.value });
                                            }}
                                        ></SubmitBox>
                                    </div>
                                )}
                                {config.submitEmail && (
                                    <div>
                                        <Text
                                            fontSize="1.2rem"
                                            fontColor={config.textColor}
                                            extra="margin-top: 20px; text-align: left; margin-left: 30px;"
                                        >
                                            Email:
                                        </Text>
                                        <SubmitBox
                                            fontSize="0.9rem"
                                            fontColor={config.textColor}
                                            extra="width: 80%;"
                                            onChange={(ev) => {
                                                this.setState({ email: ev.target.value });
                                            }}
                                        ></SubmitBox>
                                    </div>
                                )}
                                <Button
                                    id="button"
                                    fontSize="1.4rem"
                                    height="60px"
                                    extra="margin-top: 10px;"
                                    onClick={() => {
                                        let scoreData = { score: window.score };

                                        if (this.state.name != "") {
                                            scoreData["display_name"] = this.state.name;
                                        }
                                        if (this.state.email != "") {
                                            scoreData["email"] = this.state.email;
                                        }

                                        database.postScoreData(scoreData).then((res) => {
                                            this.setState({ showLeaderboard: true });

                                            database.getLeaderBoard().then((data) => {
                                                let sortedData = data.sort(
                                                    (a, b) => parseInt(a.score) < parseInt(b.score)
                                                );

                                                this.setState({ leaderboardData: sortedData });
                                            });
                                        });
                                    }}
                                >
                                    {config.submitScoreText}
                                </Button>
                            </div>
                        )}
                    </Card>
                )}

                <Card bgColor="transparent" height={card2Height} extra={`top: 50%; box-shadow: none; @media (max-width: 370px) {margin: 12px auto;}`}>
                    <Button
                        id="button"
                        height="65px"
                        fontSize="1.4rem"
                        onClick={() => {
                            window.restartGame();
                        }}
                    >
                        {config.playButtonText}
                    </Button>
                </Card>
                {(config.showText || config.showCTA) && (
                    <Card
                    bottom = "0"
                    bgColor = { config.cardColor } 
                    height = { card3Height }
                    extra = { `bottom: 10%; padding: 25px 10px; max-height: ${card3Height}px
                    transform: translate(-50%, -10%);
                    -webkit-transform: translate(-50%, -10%);
                     -moz-transform: translate(-50%, -10%);
                      -o-transform: translate(-50%, -10%);
                      -ms-transform: translate(-50%, -10%);` }
                    >

                        {config.showText && (
                            <Text fontSize="1.6rem" fontColor={config.textColor} extra="padding-bottom: 10px;">
                                {config.customText}
                            </Text>
                        )}
                        {config.showCTA && (
                            <div>
                                <Text fontSize="1.4rem" fontColor={config.textColor}>
                                    {config.ctaText}
                                </Text>
                                <Button
                                    id="button"
                                    height="60px"
                                    fontSize="1.3rem"
                                    extra="margin-top: 10px;"
                                    onClick={() => {
                                        window.open(config.ctaUrl);
                                    }}
                                >
                                    {config.ctaButtonText}
                                </Button>
                            </div>
                        )}
                    </Card>
                )}
            </div>
        );
    }
}

exports.postGameScreen = PostGameScreen;
exports.leaderboard = Leaderboard;
